class Vendedor < ActiveRecord::Base
  has_many :compras
  validates :nome, presence: true, length: { maximum: 50 }
  validates :matricula, presence: true, uniqueness: true, length: { maximum: 10 }
end
