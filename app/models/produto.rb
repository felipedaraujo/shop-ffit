class Produto < ActiveRecord::Base
  enum categoria: [:limpeza, :decoração, :eletronico]
  has_and_belongs_to_many :compras
  validates :descricao, presence: true, uniqueness: true, length: { maximum: 255 }
  validates :valor, presence: true
  validates :categoria, presence: true
end
