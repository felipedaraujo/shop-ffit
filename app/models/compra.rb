class Compra < ActiveRecord::Base
  belongs_to :vendedor
  has_and_belongs_to_many :produtos
  validates :cliente_nome, presence: true, length: { maximum: 50 }
  validates :vendedor_id, presence: true
end
