json.array!(@produtos) do |produto|
  json.extract! produto, :id, :descrição, :valor, :categoria
  json.url produto_url(produto, format: :json)
end
