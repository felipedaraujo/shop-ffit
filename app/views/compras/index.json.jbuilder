json.array!(@compras) do |compra|
  json.extract! compra, :id, :cliente_nome, :vendedor_id
  json.url compra_url(compra, format: :json)
end
