json.array!(@vendedores) do |vendedor|
  json.extract! vendedor, :id, :nome, :matricula
  json.url vendedor_url(vendedor, format: :json)
end
