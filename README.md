# Shop FFIT

### Instalação
- `git clone git@bitbucket.org:felipedaraujo/shop-ffit.git`
- `bundle install`
- `rake db:setup`

### Porquês
- Classe `Compra` sem o atributo `data_compra`: o atributo `created_at`, criado por padrão, já cumpre essa função.
- HAML: elegância.

### Próximos passos
- [ ] Validar o atributo `valor` da classe Produto`ao criar um novo produto.
- [ ] Apresentar o nome das categorias da classe `Produto` no lugar dos indices.
- [ ] Melhorar a visualização dos botões agrupados.
- [ ] Apresentar os produtos de cada compra.
- [ ] Teste, teste e teste.

### Referências
- [Como programar em português em Ruby on Rails](http://pt.stackoverflow.com/questions/19512/como-programar-em-portugu%C3%AAs-no-ruby-on-rails)
- [I18n RailsCast](http://railscasts.com/episodes/138-i18n-revised)

### Autor
[Felipe de Araújo](https://github.com/felipedaraujo)
