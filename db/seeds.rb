fernando = Vendedor.create(nome: "Fernando", matricula: 2015200300)
savio    = Vendedor.create(nome: "Sávio", matricula: 2015200400)
henrique = Vendedor.create(nome: "Henrique", matricula: 2015200500)

aspirador = Produto.create(descricao: "Aspirador", valor: 48.99, categoria: 0)
pintura   = Produto.create(descricao: "Pintura famosa", valor: 328.99, categoria: 1)
nexus     = Produto.create(descricao: "Nexus 5", valor: 978.99, categoria: 2)

felipe_compras = Compra.create(cliente_nome: "Felipe", vendedor_id: fernando.id)
felipe_compras.produtos = [aspirador]
natalia_compras = Compra.create(cliente_nome: "Natália", vendedor_id: savio.id)
natalia_compras.produtos = [aspirador, pintura]
alex_compras = Compra.create(cliente_nome: "Alex", vendedor_id: 3)
alex_compras.produtos = [aspirador, nexus]
