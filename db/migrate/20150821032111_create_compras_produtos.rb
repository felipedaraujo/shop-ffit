class CreateComprasProdutos < ActiveRecord::Migration
  def change
    create_table :compras_produtos do |t|
      t.belongs_to :compra, index: true
      t.belongs_to :produto, index: true
    end
  end
end
