class CreateCompras < ActiveRecord::Migration
  def change
    create_table :compras do |t|
      t.string :cliente_nome
      t.belongs_to :vendedor, index: true

      t.timestamps null: false
    end
    add_foreign_key :compras, :vendedores
  end
end
