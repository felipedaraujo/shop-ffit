class CreateVendedores < ActiveRecord::Migration
  def change
    create_table :vendedores do |t|
      t.string :nome
      t.integer :matricula

      t.timestamps null: false
    end
  end
end
