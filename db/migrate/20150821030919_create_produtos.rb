class CreateProdutos < ActiveRecord::Migration
  def change
    create_table :produtos do |t|
      t.string :descricao
      t.decimal :valor
      t.integer :categoria, default: 0

      t.timestamps null: false
    end
  end
end
