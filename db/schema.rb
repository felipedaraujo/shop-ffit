# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150821032111) do

  create_table "compras", force: :cascade do |t|
    t.string   "cliente_nome"
    t.integer  "vendedor_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "compras", ["vendedor_id"], name: "index_compras_on_vendedor_id"

  create_table "compras_produtos", force: :cascade do |t|
    t.integer "compra_id"
    t.integer "produto_id"
  end

  add_index "compras_produtos", ["compra_id"], name: "index_compras_produtos_on_compra_id"
  add_index "compras_produtos", ["produto_id"], name: "index_compras_produtos_on_produto_id"

  create_table "produtos", force: :cascade do |t|
    t.string   "descricao"
    t.decimal  "valor"
    t.integer  "categoria",  default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "vendedores", force: :cascade do |t|
    t.string   "nome"
    t.integer  "matricula"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
